<?php

use Illuminate\Database\Seeder;

class Bed_categorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bed_category= factory(App\Bed_category::class,5)->create();
    }
}
