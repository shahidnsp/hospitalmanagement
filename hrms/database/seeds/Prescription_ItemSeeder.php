<?php

use Illuminate\Database\Seeder;

class Prescription_ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $item= factory(App\Prescription_items::class,5)->create();
    }
}
