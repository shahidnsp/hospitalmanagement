<?php

use Illuminate\Database\Seeder;

class Operation_TypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $operation_type = factory(App\Operation_Type::class,5)->create();
    }
}
