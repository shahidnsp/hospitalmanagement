<?php

use Illuminate\Database\Seeder;

class Report_typeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $report_type = factory(\App\Report_types::class, 5)->create();
    }
}
