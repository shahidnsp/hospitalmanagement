<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $info = $this->command;
        // $this->call(UserTableSeeder::class);
       /* $info->comment('user created');
        $info->error('Username : admin Password:admin');*/
        $info->info('user table seeding started...');
        $this->call('UserSeeder');
        $info->info('Ambulance table seeding started...');
        $this->call('AmbulanceSeeder');

        $info->info('Ambulance Request table seeding started...');
        $this->call('Ambulance_RequestSeeder');

        $info->info('Bed table seeding started...');
        $this->call('BedSeeder');

        $info->info('Department table seeding started...');
        $this->call('DepartmentSeeder');

        $info->info('Doctor table seeding started...');
        $this->call('DoctorSeeder');

        $info->info('Message table seeding started...');
        $this->call('MessageSeeder');

        $info->info('Operation type table seeding started...');
        $this->call('Operation_TypeSeeder');

        $info->info('Operation table seeding started...');
        $this->call('OperationSeeder');

        $info->info('Specialization table seeding started...');
        $this->call('SpecializionSeeder');

        $info->info('Staff table seeding started...');
        $this->call('StaffSeeder');

        $info->info('Treatment table seeding started...');
        $this->call('TreatmentSeeder');

        $info->info('Patient Table seeding started... ');
        $this->call('PatientSeeder');

       $info->info('Medicine Table seeding started..');
        $this->call('MedicinSeeder');

        $info->info('Diagnoses Table seeding started..');
        $this->call('DignoseSeeder');

        $info->info('Category table seeding started...');
        $this->call('CategorySeeder');

        $info->info('Report Table seeding started...');
        $this->call('Report_typeSeeder');

        $info->info('Assing_bed Table seeding started...');
        $this->call('Assing_BedSeeder');

        $info->info('Bed_categoty Table seeding started...');
        $this->call('Bed_categorySeeder');

        $info->info('Bloodbag Table seeding started...');
        $this->call('BloodbagSeeder');

        $info->info('Donor Table seeding started...');
        $this->call('DonorSeeder');

        $info->info('Hospital Table seeding started...');
        $this->call('HospitalSeeder');

        $info->info('prescription Table seeding started...');
        $this->call('PrescriptionSeeder');

        $info->info('prescription_item Table seeding started...');
        $this->call('Prescription_ItemSeeder');
    }
}
