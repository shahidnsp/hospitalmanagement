<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePrescriptionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('presciptions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('cashhistory');
            $table->string('note');
            $table->integer('patient_id');
            $table->integer('treatment_id');
            $table->integer('hospital_id');
            $table->integer('user_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('presciptions');
    }
}
