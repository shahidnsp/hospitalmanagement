<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDignosesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('dignoses', function (Blueprint $table) {
            $table->increments('id');
            $table->string('document');
            $table->string('description',1500);
            $table->integer('patient_id');
            $table->integer('report_type_id');
            $table->integer('hospital_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('dignoses');
    }
}
