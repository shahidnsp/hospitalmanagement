<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAssignBedsTabel extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('assign_beds', function (Blueprint $table) {
            $table->increments('id');
            $table->string('status');
            $table->date('allotdate');
            $table->date('dischargedate');
            $table->string('description',1500);
            $table->integer('patient_id');
            $table->integer('bed_category_id');
            $table->integer('bed_id');
            $table->integer('staff_id');
            $table->integer('hospital_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('assign_beds');
    }
}
