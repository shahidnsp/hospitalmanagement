<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('last_name');
            $table->string('gender');
            $table->string('dob');
            $table->string('degree');
            $table->string('visiting_charge');
            $table->string('address');
            $table->string('city');
            $table->string('state');
            $table->string('office_address');
            $table->string('mobile');
            $table->string('phone');
            $table->string('email');
            $table->integer('user_id');
            $table->integer('hospitals_id');
            $table->integer('department_id');
            $table->integer('specialization_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('doctors');
    }
}
