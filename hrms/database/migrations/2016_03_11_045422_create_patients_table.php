<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id');
            $table->string('patientcode');
            $table->string('firstname');
            $table->string('lastname');
            $table->string('gender');
            $table->string('dob');
            $table->string('bloodgroup');
            $table->string('address');
            $table->string('state');
            $table->string('country');
            $table->string('mobile');
            $table->string('phone');
            $table->string('email');
            $table->string('gfirstname');
            $table->string('glastname');
            $table->string('ggender');
            $table->string('gaddress');
            $table->string('gcity');
            $table->string('gmobile');
            $table->string('gphone');
            $table->string('relationship');
            $table->date('admitdate');
            $table->time('admittime');
            $table->string('status');
            $table->integer('hospital_id');
            $table->integer('doctor_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('patients');
    }
}
