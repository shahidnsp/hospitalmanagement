<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOperationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('operations', function (Blueprint $table) {
            $table->increments('id');
            $table->date('operation_date');
            $table->time('operation_time');
            $table->string('description');
            $table->string('operation_charge');
            $table->integer('patients_id');
            $table->integer('operation_type_id');
            $table->integer('bed_category_id');
            $table->integer('beds_id');
            $table->integer('hospitals_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('operations');
    }
}
