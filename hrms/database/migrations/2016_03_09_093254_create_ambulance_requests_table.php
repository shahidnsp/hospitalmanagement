<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAmbulanceRequestsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ambulance_requests', function (Blueprint $table) {
            $table->increments('id');
            $table->string('address');
            $table->string('charge');
            $table->date('date');
            $table->time('time');
            $table->string('dispatch');
            $table->integer('ambulances_id');
            $table->integer('patient_id');
            $table->integer('hospitals_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('ambulance_requests');
    }
}
