<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

$factory->define(App\User::class, function (Faker\Generator $faker) {
    return [
        'username' => $faker->name,
        'password' => bcrypt(str_random(10)),
        'firstname' => $faker->name,
        'lastname' => $faker->name,
        'gender' => $faker->randomElement(['male', 'female']),
        'address' => $faker->address,
        'mobile' => $faker->phoneNumber,
        'phone' => $faker->phoneNumber,
        'department'=>$faker->name,
        'email' => $faker->email,
        'user_type'=>$faker->randomElement(['a','u']),
        'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'remember_token' => str_random(10),
    ];
});
//ambulance

$factory->define(App\Ambulance::class, function (Faker\Generator $faker) {
    return [
        'regno'=>$faker->creditCardNumber,
        'drivername'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'hospitals_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),

    ];
});

//ambulance_request
$factory->define(App\Ambulance_Request::class, function (Faker\Generator $faker) {
    return [
        'address'=>$faker->address,
        'charge'=>$faker->randomNumber(),
        'date'=>$faker->date('Y-m-d'),
        'time'=>$faker->time('H:i:s'),
        'dispatch'=>$faker->date('Y-m-d'),
        'ambulances_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'hospitals_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'patient_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),

    ];
});

//message

$factory->define(App\Message::class, function (Faker\Generator $faker) {
    return [
        'subjects' => $faker->text,
        'content' => $faker->paragraph,
        'user_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});

//operation
$factory->define(App\Operation::class, function (Faker\Generator $faker) {
    return [
        'operation_date' => $faker->date('Y-m-d'),
        'operation_time' => $faker->time('H:i:s'),
        'description' => $faker->sentence(2),
        'operation_charge' => $faker->randomNumber(),
        'patients_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'operation_type_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'bed_category_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});

//doctor
$factory->define(App\Doctors::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'gender' => $faker->randomElement(['male', 'female']),
        'dob' => $faker->date('Y-m-d'),
        'degree' => $faker->randomElement(['MBBS','BDS','BHMS']),
        'visiting_charge' => $faker->randomNumber(),
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->city,
        'office_address' => $faker->address,
        'mobile' => $faker->phoneNumber,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'user_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'department_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'specialization_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        ];
});

//staff
$factory->define(App\Staffs::class, function (Faker\Generator $faker) {
    return [
        'first_name' => $faker->name,
        'last_name' => $faker->name,
        'gender' => $faker->randomElement(['male', 'female']),
        'dob' => $faker->date('Y-m-d'),
        'address' => $faker->address,
        'city' => $faker->city,
        'state' => $faker->city,
        'staff_type'=>$faker->randomElement(['supporting_staff','nurse','accountant','attender']),
        'mobile' => $faker->phoneNumber,
        'phone' => $faker->phoneNumber,
        'email' => $faker->email,
        'user_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'department_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),

    ];
});

//specialization

$factory->define(App\Specialization::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});

//department
$factory->define(App\Departments::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});

//treatment
$factory->define(App\Treatments::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});

//operation type
$factory->define(App\Operation_Type::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});
//bed
$factory->define(App\Beds::class, function (Faker\Generator $faker) {
    return [
        'bed_number'=>$faker->randomNumber(),
        'charge'=>$faker->randomNumber(),
        'description' => $faker->sentence(2),
        'bed_category_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
        'hospitals_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});

//patients
$factory->define(App\Patients::class, function (Faker\Generator $faker) {
    return [
        'patientcode' => $faker->randomNumber,
        'firstname'=>$faker->firstName,
        'lastname' =>$faker->lastName,
        'gender' =>$faker->randomElement(['Male','Female']),
        'bloodgroup' =>$faker->randomElement(['A+', 'A-','B+', 'B-', 'O+', 'O-', 'AB+', 'AB-']),
        'address' =>$faker->address,
        'state' =>$faker->state,
        'country' =>$faker->country,
        'mobile' =>$faker->phoneNumber,
        'phone' =>$faker->phoneNumber,
        'email' =>$faker->email,
        'gfirstname' =>$faker->firstName,
        'glastname' =>$faker->lastName,
        'ggender' =>$faker->randomElement(['male', 'Female']),
        'gaddress' =>$faker->address,
        'gcity' =>$faker->city,
        'gmobile' =>$faker->phoneNumber,
        'gphone' =>$faker->phoneNumber,
        'relationship' =>$faker->randomElement(['father','mother', 'sister']),
        'admitdate' =>$faker->date(),
        'admittime' =>$faker->time(),
        'status' =>$faker->randomElement(['discharge','death', 'recovery', 'cured', 'operation', 'under treatment', 'Admit']),
        'hospital_id' =>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'doctor_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
    ];
});

//medicine

$factory->define(App\Medicines::class, function (Faker\Generator $faker){
    return [
        'name' => $faker->name,
       'salesprice' => '20',
         'purchaseprice' => '15',
        'supplier' => $faker->company,
       'description' => $faker->sentence(2),
        'quantity' => '5',
        'category_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'hospital_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9]),
    ];
});

//Dignoses

$factory->define(App\Diagnoses::class, function (Faker\Generator $faker) {
    return [
        'document'=> $faker->word,
        'description'=> $faker->sentence(),
        'patient_id'=> $faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'report_type_id'=> $faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'hospital_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9])
    ];
});

//Categories

$factory->define(App\Categories::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'hospital_id' => $faker->randomElement([1,2,3,4,5,6,7,8,9])
    ];
});

//report_types

$factory->define(App\Report_types::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9])
    ];
});

//assing_bed
$factory->define(App\Assign_beds::class, function (Faker\Generator $faker) {
    return [
        'status'=>$faker->text(),
        'allotdate'=>$faker->date('Y-m-d'),
        'dischargedate'=>$faker->date('Y-m-d'),
        'description'=>$faker->text(),
        'patient_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'bed_category_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'bed_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'staff_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9])
    ];
});
//prescription
$factory->define(App\Prescriptions::class, function (Faker\Generator $faker) {
    return [
        'cashhistory'=>$faker->sentence(2),
        'note'=>$faker->sentence(2),
        'patient_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'treatment_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'user_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
    ];
});
//prescription_item
$factory->define(App\Prescription_items::class, function (Faker\Generator $faker) {
    return [
        'nooftime'=>$faker->randomElement([1,2,3]),
        'prescription_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'medicine_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
    ];
});

//hospital
$factory->define(App\Hospitals::class, function (Faker\Generator $faker) {
    return [
        'accountno'=>$faker->randomNumber(),
        'name'=>$faker->name,
        'address'=>$faker->address,
        'phone'=>$faker->phoneNumber,
        'mobile'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'website'=>$faker->url
    ];
});

//donor
$factory->define(App\Donors::class, function (Faker\Generator $faker) {
    return [
        'name'=>$faker->name,
        'gender'=>$faker->randomElement(['male', 'Female']),
        'age'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'phone'=>$faker->phoneNumber,
        'email'=>$faker->email,
        'group'=>$faker->text,
        'lastdate'=>$faker->date('Y-m-d'),
        'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
    ];
});

//bloodbag
$factory->define(App\Bloodbags::class, function (Faker\Generator $faker) {
    return [
        'group'=>$faker->text(),
        'noofbags'=>$faker->randomElement([1,2,3,4,5,6,7,8,9]),
        'hospital_id'=>$faker->randomElement([1,2,3,4,5,6,7,8,9])
    ];
});

//bed_category
$factory->define(App\Bed_category::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'hospital_id' => $faker->randomElement([1, 2, 3, 4, 5, 6, 7, 8, 9]),
    ];
});

