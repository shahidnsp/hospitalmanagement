<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctors extends Model
{
    //
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'dob',
        'degree',
        'visiting_charge',
        'address',
        'city',
        'state',
        'office_address',
        'mobile',
        'phone',
        'email',
        'user_id',
        'hospitals_id',
        'department_id',
        'specialization_id'
    ];
    public function user()
    {
        return $this->belongsTo('App\User');
    }
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
    public function department()
    {
        return $this->belongsTo('App\Departments');
    }
    public function specialization()
    {
        return $this->belongsTo('App\Specialization');
    }


    public function Patient()
    {
        return $this->hasMany('App\Patients');
    }
}
