<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Categories extends Model
{
    protected $fillable = [
        'name',
        'hospital_id'
    ];
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
    public function medicine()
    {
        return $this->hasMany('App\Medicines');
    }
}
