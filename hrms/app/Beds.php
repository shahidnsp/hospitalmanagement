<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Beds extends Model
{
    //
    protected $fillable = ['bed_number','charge','description','bed_category_id','hospitals_id'];

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals','hospitals_id');
    }
    public function bed_category()
    {
        return $this->belongsTo('App\Bed_category');
    }
}
