<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Departments extends Model
{
    //
    protected $fillable = ['name', 'hospitals_id'];


    public function hospital()
    {
        return $this->belongsTo('App\Hospitals','hospitals_id');
    }
    public function doctor()
    {
        return $this->hasMany('App\Doctors');
    }
}