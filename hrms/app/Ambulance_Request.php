<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ambulance_Request extends Model
{
    protected $table='ambulance_requests';
    //
    protected $fillable = ['address','charge','date','time','dispatch','ambulance_id','hospitals_id','patient_id'];

    public function ambulance()
    {
        return $this->belongsTo('App\Ambulance','ambulances_id');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals','hospitals_id');
    }
    public function patient()
    {
        return $this->belongsTo('App\User');
    }

}
