<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    public function message()
    {
        return $this->hasMany('App\Message');
    }
    public function doctor()
    {
        return $this->hasMany('App\Doctors');
    }


    public function prescription()
    {
        return $this->hasMany('App\Prescriptions');
    }
}
