<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Donors extends Model
{
    protected $fillable = [
        'name',
        'gender',
        'age',
        'phone',
        'email',
        'group',
        'lastdate',
        'hospital_id'
    ];
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
}
