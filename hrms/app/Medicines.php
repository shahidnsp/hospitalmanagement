<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Medicines extends Model
{
    protected $fillable = [
        'name',
        'salesprice',
        'purchaseprice',
        'supplier',
        'description',
        'quantity',
        'category_id',
        'hospital_id',
    ];

    public function category()
    {
        return $this->belongsTo('App\Categories');
    }
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }



    public function prescription_item()
    {
        return $this->hasMany('App\Prescription_item');

    }
}
