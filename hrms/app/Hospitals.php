<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Hospitals extends Model
{
    //
    protected $table='hospitals';
    protected $fillable = [
        'accountno',
        'name',
        'address',
        'phone',
        'mobile',
        'email',
        'website'
    ];

    public function ambulance()
    {
        return $this->hasMany('App\Ambulance');
    }
    public function ambulance_request()
    {
        return $this->hasMany('App\Ambulance_Request');
    }
    public function bed_category()
    {
        return $this->hasMany('App\Bed_category','hospital_id');
    }
    public function bed()
    {
        return $this->hasMany('App\Beds','hospitals_id');
    }
    public function bloodbag()
    {
        $this->hasMany('App\Bloodbags');
    }
    public function category()
    {
        return $this->hasMany('App\Categories');
    }
    public function department()
    {
        return $this->hasMany('App\Departments','hospitals_id');
    }
    public function dignos()
    {
        return $this->hasMany('App\Diagnoses');
    }
    public function doctor()
    {
        return $this->hasMany('App\Doctors');
    }
    public function donor()
    {
        return $this->hasMany('App\Donors');
    }
    public function medicine()
    {
        return $this->hasMany('App\Medicines');
    }
    public function message()
    {
        return $this->hasMany('App\Message','hospitals_id');
    }


    public function operation()
    {
        return $this->hasMany('App\Operation');
    }

    public function operation_type()
    {
        return $this->hasMany('App\Operation_type');
    }


    public function Patient()
    {
        return $this->hasMany('App\Patients');
    }


    public function prescription_item()
    {
        return $this->hasMany('App\Prescription_item');

    }

    public function prescription()
    {
        return $this->hasMany('App\Prescriptions');
    }

    public function report_type()
    {
        return $this->hasMany('App\Hospitals');
    }

    public function specialization()
    {
        return $this->hasMany('App\Specialization');
    }

    public function treatment()
    {
        return $this->hasMany('App\Treatment');
    }


}
