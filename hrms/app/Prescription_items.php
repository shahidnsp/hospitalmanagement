<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescription_items extends Model
{
    protected $fillable = [
        'nooftime',
        'prescription_id',
        'medicine_id',
        'hospital_id'
    ];


    public function prescription()
    {
        return $this->belongsTo('App\Presciptions');
    }

    public function medicine()
    {
        return $this->belongsTo('App\Medicines');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }

}
