<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Staffs extends Model
{
    //
    protected $fillable = [
        'first_name',
        'last_name',
        'gender',
        'dob',
        'address',
        'city',
        'state',
        'staff_type',
        'mobile',
        'phone',
        'email',
        'user_id' ,
        'hospitals_id',
        'department_id',
        'specialization_id',
    ];

    public function specialization()
    {
        return $this->belongsTo('App\Specialization');
    }
}
