<?php

namespace App\Http\Controllers\Auth;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Doctors;
use App\Http\Requests;

class DoctorsController extends Controller
{
    /**
     * validation
     * @param array $data
     * @return mixed
     */

    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'first_name' =>'required',
                'gender' =>'required',
                'degree' =>'required',
                'visiting_charge' =>'required',
                'office_address' =>'required',
                'mobile' =>'required',
                'user_id' =>'required',
                'hospitals_id' =>'required',
                'department_id' =>'required',
                'specialization_id' =>'required',
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Doctors::with('hospital','user','department','specialization')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $doctor = new Doctors($request->all());

        if($doctor->save())
        {
            return $doctor;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $doctor = Doctors::find($id);

        if($doctor != null) {
            $doctor->fill($request->all());

            if ($doctor->save())
            {
                return $doctor;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Recode not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Doctors::destroy($id))
        {
            return Response::json(['msg'=>'Doctor delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
