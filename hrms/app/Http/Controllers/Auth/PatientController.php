<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use Validator;
use App\Patients;
use App\Http\Requests;

class PatientController extends Controller
{
    /**
     * validation
     * @param array $data
     * @return mixed
     */

    public function validetor(array $data)
    {
        return Validator::make($data,
            [
                'patientcode'=>'required',
                'firstname'=>'required',
                'gender'=>'required',
                'bloodgroup'=>'required',
                'address'=>'required',
                'mobile'=>'required',
                'gfirstname'=>'required',
                'ggender'=>'required',
                'gaddress'=>'required',
                'gmobile'=>'required',
                'relationship'=>'required',
                'admitdate'=>'required',
                'admittime'=>'required',
                'status'=>'required',
                'hospital_id'=>'required',
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Patients::with('prescriptions')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $patient= new Patients($request->all());

        if($patient->save())
        {
            return $patient;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $patient = Patients::find($id);

        if($patient != null) {
            $patient->fill($request->all());

            if ($patient->save())
            {
                return $patient;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Patients::destroy($id))
        {
            return Response::json(['msg'=>'Patient delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
