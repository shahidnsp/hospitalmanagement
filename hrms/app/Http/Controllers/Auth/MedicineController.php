<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validation;
use App\Medicines;

use App\Http\Requests;

class MedicineController extends Controller
{
    public function validator(array $data)
    {
        return Validation::make($data,
            [
                'name'=>'required',
                'salesprice'=>'required',
                'purchaseprice'=>'required',
                'supplier'=>'required',
                'quantity'=>'required',
                'category_id'=>'required',
                'hospital_id'=>'required',
            ]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Medicines::with('hospital','category')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $medicin =new Medicines($request->all());
        if($medicin->save())
        {
            return $medicin;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $medicin = Medicines::find($id);
        if($medicin != null)
        {
            $medicin->fill($request->all());
            if($medicin->save())
            {
                return $medicin;
            }
            else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Medicines::destroy($id))
        {
            return Response::json(['msg'=>'medicine delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
