<?php

namespace App\Http\Controllers;

use App\Hospitals;
use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;

class Hospital_Controller extends Controller
{
    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'accountno' => 'required',
                'name' => 'required',
                'address' => 'required',
                'phone' => 'required',
                'mobile' => 'required'
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Hospitals::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $hospital = new Ambulance($request->all());

        if($hospital->save())
        {
            return $hospital;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $hospital = Ambulance::find($id);

        if($hospital != null) {
            $hospital->fill($request->all());

            if ($hospital->save())
            {
                return $hospital;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Hospitals::destroy($id))
        {
            return Response::json(['msg'=>'hospital deleted']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
