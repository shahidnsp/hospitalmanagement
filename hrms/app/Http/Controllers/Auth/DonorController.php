<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  Validator;
use  App\Donors;
use App\Http\Requests;

class DonorController extends Controller
{
    /**
     * validator
     * @param array $data
     * @return mixed
     */
    public function validetor(array $data)
    {
        return Validator::make($data,
            [
                'name'=>'required',
                'gender'=>'required',
                'age'=>'required',
                'phone'=>'required',
                'group'=>'required',
                'lastdate'=>'required',
                'hospital_id'=>'required',
            ]);
    }



    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Donors::with('hospital')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $donor = new Donors($request->all());
        if($donor->save())
        {

            return $donor;
        }

        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $donor = Donors::find($id);

        if($donor != null) {
            $donor->fill($request->all());

            if ($donor->save())
            {
                return $donor;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {

        if(Donors::destroy($id))
        {
            return Response::json(['msg'=>'Donor delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
