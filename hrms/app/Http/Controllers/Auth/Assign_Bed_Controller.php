<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Response;
use Validator;
use App\Assign_beds;

class Assign_Bed_Controller extends Controller
{

    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'status' => 'required',
                'allotedate' => 'required',
                'patient_id' => 'required',
                'bed_catogery_id' => 'required',
                'bed_id' => 'required',
                'staff_id' => 'required',
                'hospital_id' => 'required',
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Assign_beds::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $assign_bed = new Assign_beds($request->all());

        if ($assign_bed->save()) {
            return $assign_bed;
        } else {
            return Response::json(['error' => 'Server down!'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if ($validator->fails()) {
            return Response::json($validator->errors(), 400);
        }

        $assign_bed = Assign_beds::find($id);

        if ($assign_bed != null) {
            $assign_bed->fill($request->all());

            if ($assign_bed->save()) {
                return $assign_bed;
            } else {
                return Response::json(['error' => 'Server Down'], 500);
            }
        } else {
            return Response::json(['error' => 'Record Not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if (Assign_beds::destroy($id)) {
            return Response::json(['msg' => 'Assign bed deleted']);
        } else {
            return Response::json(['error' => 'Record not found']);
        }
    }
}
