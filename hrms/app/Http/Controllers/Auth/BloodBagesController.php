<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Bloodbags;
use App\Http\Requests;

class BloodBagesController extends Controller
{
    /**
     * validation
     * @param array $data
     * @return mixed
     */
    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'group'=>'required',
                'noofbag'=>'required',
                'hospital_id'=>'required',
            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return  Bloodbags::with('hospital')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $blood =new Bloodbags($request->all());
        if($blood->save())
        {
            return $blood;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $blood = Bloodbags::find($id);
        if($blood != null)
        {
            $blood->fill($request->all());
            if($blood->save())
            {
                return $blood;
            }
            else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Bloodbags::destroy($id))
        {
            return Response::json(['msg'=>'Blood bag delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
