<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use Validator;

use App\Prescriptions;

use App\Http\Controllers\Controller;

class Prescription_Controller extends Controller
{

    /**
     * @param array $data
     * @return mixed
     */

    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'cashhistory' => 'required',
                'patient_id' => 'required',
                'treatment_id' => 'required',
                'hospital_id' => 'required',
                'user_id' => 'required'
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Prescriptions::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $prescription = new Ambulance($request->all());

        if($prescription->save())
        {
            return $prescription;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $presciption = Ambulance::find($id);

        if($presciption != null) {
            $presciption->fill($request->all());

            if ($presciption->save())
            {
                return $presciption;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Prescriptions::destroy($id))
        {
            return Response::json(['msg'=>'Prescription deleted']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
