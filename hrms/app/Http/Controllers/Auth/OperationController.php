<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use Validation;
use App\Operation;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class OperationController extends Controller
{

    /**
     * validation
     * @param array $data
     * @return mixed
     */

    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'operation_date'=>'required',
                'operation_time'=>'required',
                'patient_id'=>'required',
                'operation_type_id'=>'required',
                'bed_category_id'=>'required',
                'hospitals_id' =>'required',

            ]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Operation::all();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $operation= new Operation($request->all());

        if($operation->save())
        {
            return $operation;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $operation = Operation::find($id);

        if($operation != null) {
            $operation->fill($request->all());

            if ($operation->save())
            {
                return $operation;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Operation::destroy($id))
        {
            return Response::json(['msg'=>'Operation delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
