<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;
use App\Diagnoses;
use App\Http\Requests;

class DignosesController extends Controller
{
    /**
     * @param array $data
     * @return mixed
     * validation
     */

    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'document'=>'required',
                'patient_id'=>'required',
                'hospital_id'=>'required',
            ]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Diagnoses::with('hospital','patient','report_type')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $dignos = new Diagnoses($request->all());
        if($dignos->save())
        {

            return $dignos;
        }

        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());
        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }
        $dignos = Diagnoses::find($id);

        if($dignos != null) {
            $dignos->fill($request->all());

            if ($dignos->save())
            {
                return $dignos;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Diagnoses::destroy($id))
        {
            return Response::json(['msg'=>'Dignose  delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
