<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Ambulance;
use Validator;
use App\Http\Requests;
use App\Http\Controllers\Controller;
class AmbulanceController extends Controller
{

    /**
     * validation
     * @param array $data
     * @return mixed
     */
    public function validator(array $data)
    {
        return Validator::make($data,
            [
                'regno'=>'required',
                'drivername'=>'required',
                'phone'=>'required',
                'hospitals_id'=>'required',
        ]);
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Ambulance::with('hospital','ambulance_request')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $ambulance = new Ambulance($request->all());

        if($ambulance->save())
        {
            return $ambulance;
        }
        else
        {
            return Response::json(['error' => 'server is down'], 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = $this->validator($request->all());

        if($validator->fails())
        {
            return Response::json($validator->errors(),400);
        }

        $ambulance = Ambulance::find($id);

        if($ambulance != null) {
            $ambulance->fill($request->all());

            if ($ambulance->save())
            {
                return $ambulance;
            } else
            {
                return Response::json(['error' => 'server is down'], 500);
            }
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(Ambulance::destroy($id))
        {
            return Response::json(['msg'=>'ambulance delete']);
        }
        else
        {
            return Response::json(['error'=>'Record not found']);
        }
    }
}
