<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web']], function () {
    //
});

Route::group(['prefix' => 'user'], function () {

    Route::get('login', ['as' => 'login', 'uses' => 'Auth\AuthController@getLogin']);
    Route::post('login', ['as' => 'login', 'uses' => 'Auth\AuthController@postLogin']);
    Route::get('logout', ['as' => 'logout', 'uses' => 'Auth\AuthController@getLogout']);
});

Route::group(['prefix' => 'api'], function ()
{
//Route::group(['middleware'=>'auth','prefix'=>'api'],function(){
    Route::resource('patient','Auth\PatientController');
    Route::resource('medicine','Auth\MedicineController');
    Route::resource('dignose','Auth\DignosesController');
    Route::resource('category','Auth\CategoryController');
    Route::resource('report_type','Auth\Report_TypeController');
    Route::resource('donor','Auth\DonorController');
    Route::resource('bloodbag','Auth\BloodBagesController');
    Route::resource('bed_category','Auth\Bed_CategoryController');
    Route::resource('message','Auth\MessageController');
    Route::resource('operation','Auth\OperationController');
    Route::resource('ambulance','Auth\AmbulanceController');
    Route::resource('ambulance_request','Auth\Ambulance_RequestController');
    Route::resource('doctors','Auth\DoctorsController');
    Route::resource('staff','Auth\StaffController');
    Route::resource('treatment','Auth\TreatmentController');
    Route::resource('operation_type','Auth\Operation_TypeController');
    Route::resource('bed','Auth\BedController');
    Route::resource('department','Auth\DepartmentController');
    Route::resource('specialization','Auth\SpecializationController');

});

Route::get('template/{name}', ['as' => 'template', function ($name) {
    return view('admin.' . $name);
}]);



// Catch all undefined routes and give to index.php to handle  .
Route::any('{undefinedRoute}', function ($undefinedRoute) {
    return view('index');
})->where('undefinedRoute', '([A-z\d-\/_.]+)?');


// Using different syntax for Blade to avoid conflicts with Jade.
// You are well-advised to go without any Blade at all.
Blade::setContentTags('[[', ']]'); // For variables and all things Blade.
Blade::setEscapedContentTags('[[-', '-]]'); // For escaped data.
