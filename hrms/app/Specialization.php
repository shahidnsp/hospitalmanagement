<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specialization extends Model
{
    //
    protected $fillable = ['name','hospitals_id'];
    public function doctor()
    {
        return $this->hasMany('App\Doctors');
    }
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }

    public function staff()
    {
        return $this->hasMany('App\Staffs');
    }
}
