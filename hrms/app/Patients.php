<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Patients extends Model
{
    protected $table='patients';

    protected $fillable = [
        'patientcode',
        'firstname',
        'lastname',
        'gender',
        'bloodgroup',
        'address',
        'state',
        'country',
        'mobile',
        'phone',
        'email',
        'gfirstname',
        'glastname',
        'ggender',
        'gaddress',
        'gcity',
        'gmobile',
        'gphone',
        'relationship',
        'admitdate',
        'admittime',
        'status',
        'hospital_id',
        'doctor_id'
    ];


    public function prescriptions()
    {
        return $this->hasMany('App\Prescriptions','patient_id');
    }
    public function ambulance_request()
    {
        return $this->hasMany('App\Ambulance_request');

    }
    public function dignos()
    {
        return $this->hasMany('App\Diagnoses');
    }

    public function operation()
    {
        return $this->hasMany('App\Operation');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }


    public function doctor()
    {
        return $this->belongsTo('App\Doctors');
    }

    public function prescription()
    {
        return $this->hasMany('App\Prescriptions');
    }
}
