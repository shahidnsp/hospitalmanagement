<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report_types extends Model
{
    protected $fillable = [
        'name',
        'hospital_id'
    ];
    public function Dignose()
    {
        $this->hasMany('App\Daignoses');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
}
