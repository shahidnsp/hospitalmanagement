<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Treatments extends Model
{
    //
    protected $table ='treatements';
    protected $fillable = ['name','hospitals_id'];

    public function prescription()
    {
        return $this->hasMany('App\Prescriptions');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
}
