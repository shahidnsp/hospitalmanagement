<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation_Type extends Model
{
    //
    protected  $table='operation_types';
    protected $fillable = ['name','hospitals_id'];


    public function operation()
    {
        return $this->hasMany('App\Operation');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
}
