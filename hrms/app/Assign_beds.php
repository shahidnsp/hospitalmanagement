<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Assign_beds extends Model
{
    protected $fillable = [
        'status',
        'allotdate',
        'dischargedate',
        'description',
        'patients_id',
        'bed_category_id',
        'bed_id',
        'staff_id',
        'hospital_id'
    ];
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
    public function patient()
    {
        return $this->belongsTo('App\Patients');
    }
    public function bed_category()
    {
        return $this->belongsTo('App\Bed_category');
    }
    public function bed()
    {
        return $this->belongsTo('App\Beds');
    }
    public function staff()
    {
        return $this->belongsTo('App\Staffs');
    }

}
