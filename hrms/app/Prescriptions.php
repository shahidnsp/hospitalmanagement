<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Prescriptions extends Model
{
    protected $table='presciptions';
    protected $fillable = [
        'cashhistory',
        'note',
        'patient_id',
        'treatment_id',
        'hospital_id',
        'user_id',
    ];

    public function patients()
    {
        return $this->belongsTo('App\Patients','patient_id');
    }


    public function treatment()
    {
        return $this->belongsTo('App\Treatments');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function prescription_item()
    {
        return $this->hasMany('App\Prescription_item');

    }
}
