<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Diagnoses extends Model
{
    protected $table = 'dignoses';
    protected $fillable = [
        'document',
        'description',
        'patient_id',
        'report_type_id',
        'hospital_id'
    ];

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
    public function patient()
    {
        return $this->belongsTo('App\Patients');
    }

    public function report_type()
    {
        return $this->belongsTo('App\Report_types');
    }
}
