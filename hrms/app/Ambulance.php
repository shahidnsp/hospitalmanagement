<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ambulance extends Model
{

    protected $fillable = ['regno', 'drivername','address','phone','hospitals_id'];

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals','hospitals_id');
    }
    public function ambulance_request()
    {
        return $this->hasMany('App\Ambulance_Request','ambulances_id');
    }
}
