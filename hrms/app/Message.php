<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    //
    protected $fillable = [
        'subjects',
        'content',
        'user_id',
        'hospitals_id',
    ];
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals','hospitals_id');
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
