<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bloodbags extends Model
{
    protected $fillable = [
        'group',
        'noofbags',
        'hospital_id'
    ];
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }
}
