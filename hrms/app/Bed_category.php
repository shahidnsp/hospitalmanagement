<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bed_category extends Model
{
    protected $fillable = [
        'name',
        'hospital_id'
    ];
    public function hospital()
    {
        return $this->belongsTo('App\Hospitals','hospital_id');
    }
    public function bed()
    {
        return $this->hasMany('App\Beds');
    }

    public function operation()
    {
        return $this->hasMany('App\Operation');
    }

}
