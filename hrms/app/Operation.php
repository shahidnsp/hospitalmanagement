<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Operation extends Model
{
    //
    protected $fillable = [
        'operation_date',
        'operation_time',
        'description',
        'operation_charge',
        'patient_id',
        'operation_type_id',
        'bed_category_id',
        'hospitals_id' ,
    ];

    public function patient()
    {
        return $this->belongsTo('App\Patients');
    }

    public function operation_type()
    {
        return $this->belongsTo('App\Operation_type');
    }

    public function bed_category()
    {
        return $this->belongsTo('App\Bed_category');
    }

    public function hospital()
    {
        return $this->belongsTo('App\Hospitals');
    }

}
